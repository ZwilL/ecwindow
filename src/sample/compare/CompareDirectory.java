package sample.compare;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CXinZhi on 2016/10/20.
 */
public class CompareDirectory {

	// 文件夹级别
	private int level;

	// 文件夹名称
	private String directoryName;

	// 文件夹绝对路径
	private String directoryPath;

	//文件夹相对对路径
	private String relatPath;


	// 当前目录下是否存在子目录
	private List<CompareDirectory> childDirectorys;

	// 当前目录下存在的文件
	private List<CompareFile> fileList;

	public CompareDirectory(Integer level, String name, String path,String relatPath) {
		this.level = level;
		this.directoryName = name;
		this.directoryPath = path;
		this.relatPath = relatPath;
		fileList = new ArrayList<CompareFile>();
		childDirectorys = new ArrayList<CompareDirectory>();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getDirectoryName() {
		return directoryName;
	}

	public void setDirectoryName(String directoryName) {
		this.directoryName = directoryName;
	}

	public String getDirectoryPath() {
		return directoryPath;
	}

	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	public List<CompareDirectory> getChildDirectorys() {
		return childDirectorys;
	}

	public void setChildDirectorys(List<CompareDirectory> childDirectorys) {
		this.childDirectorys = childDirectorys;
	}


	public String getRelatPath() {
		return relatPath;
	}

	public void setRelatPath(String relatPath) {
		this.relatPath = relatPath;
	}

	public List<CompareFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<CompareFile> fileList) {
		this.fileList = fileList;
	}

}
